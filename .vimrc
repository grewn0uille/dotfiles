:set nu

:set colorcolumn=80
:set tw=80

filetype plugin indent on

:colorscheme torte
:filetype plugin on

:execute pathogen#infect()

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:pymode_python = 'python3'

syntax enable
au BufRead,BufNewFile *.html set filetype=html
au BufRead,BufNewFile *.jinja2 set filetype=html

nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>


nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>
set showmode


source ~/.vimrc.bepo

" flake8 python-mode bats.vim syntastic vim-python-pep8-indent vim-fugitive fzf